<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p dir="auto"><a href="https://github.com/openmv/openmv/actions/workflows/firmware.yml"><img src="https://github.com/openmv/openmv/actions/workflows/firmware.yml/badge.svg" alt="固件构建🔥" style="max-width: 100%;"></a>
<a href="https://github.com/openmv/openmv/blob/master/LICENSE"><img src="https://camo.githubusercontent.com/be91f226c1973a868734bc941028e0468993af5d1546cf4e417cfc0bbcd3e120/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6963656e73652f6f70656e6d762f6f70656e6d763f6c6162656c3d6c6963656e7365253230254532253941253936" alt="GitHub 许可证" data-canonical-src="https://img.shields.io/github/license/openmv/openmv?label=license%20%E2%9A%96" style="max-width: 100%;"></a>
<a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/1e0fdc59190c29ecd693699c0e630bdffb1294063228eb91b9e8f265ec9778e1/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f762f72656c656173652f6f70656e6d762f6f70656e6d763f736f72743d73656d766572"><img src="https://camo.githubusercontent.com/1e0fdc59190c29ecd693699c0e630bdffb1294063228eb91b9e8f265ec9778e1/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f762f72656c656173652f6f70656e6d762f6f70656e6d763f736f72743d73656d766572" alt="GitHub 版本（最新 SemVer）" data-canonical-src="https://img.shields.io/github/v/release/openmv/openmv?sort=semver" style="max-width: 100%;"></a>
<a href="https://github.com/openmv/openmv/network"><img src="https://camo.githubusercontent.com/0e5cc1dc1e893a7b06961fa073e1c349ff7c50aba6995f0c981a4fa18095ef26/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f666f726b732f6f70656e6d762f6f70656e6d763f636f6c6f723d677265656e" alt="GitHub 分叉" data-canonical-src="https://img.shields.io/github/forks/openmv/openmv?color=green" style="max-width: 100%;"></a>
<a href="https://github.com/openmv/openmv/stargazers"><img src="https://camo.githubusercontent.com/afca142ecba68e5424b20430d6aa352e942f91f54b01a7636a171cabf200e27c/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f73746172732f6f70656e6d762f6f70656e6d763f636f6c6f723d79656c6c6f77" alt="GitHub 星星" data-canonical-src="https://img.shields.io/github/stars/openmv/openmv?color=yellow" style="max-width: 100%;"></a>
<a href="https://github.com/openmv/openmv/issues"><img src="https://camo.githubusercontent.com/75e2daf85dbdc0056ee72df513b0abf94c2980cab5df48d243e51be0a2ee75ef/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6973737565732f6f70656e6d762f6f70656e6d763f636f6c6f723d6f72616e6765" alt="GitHub 问题" data-canonical-src="https://img.shields.io/github/issues/openmv/openmv?color=orange" style="max-width: 100%;"></a></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/openmv/openmv-media/master/logos/openmv-logo/logo.png"><img width="480" src="https://raw.githubusercontent.com/openmv/openmv-media/master/logos/openmv-logo/logo.png" style="max-width: 100%;"></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开源机器视觉项目</font></font></h1><a id="user-content-the-open-source-machine-vision-project" class="anchor" aria-label="永久链接：开源机器视觉项目" href="#the-open-source-machine-vision-project"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="#overview"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">概述</font></font></a></li>
<li><a href="#tensorflow-support"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TensorFlow 支持</font></font></a></li>
<li><a href="#interface-library"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">接口库</font></font></a>
<ul dir="auto">
<li><a href="#note-on-serial-port"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">串行端口注意事项</font></font></a></li>
</ul>
</li>
<li><a href="#building-the-firmware-from-source"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从源代码构建固件</font></font></a></li>
<li><a href="#contributing-to-the-project"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为项目做贡献</font></font></a>
<ul dir="auto">
<li><a href="#contribution-guidelines"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献指南</font></font></a></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">概述</font></font></h2><a id="user-content-overview" class="anchor" aria-label="固定链接：概述" href="#overview"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenMV 项目旨在通过开发用户友好、开源、低成本的机器视觉平台，让初学者更容易掌握机器视觉。OpenMV 摄像头可使用 Python3 进行编程，并配备大量机器学习和图像处理功能，例如人脸检测、关键点描述符、颜色跟踪、QR 和条形码解码、AprilTags、GIF 和 MJPEG 录制等。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenMV Cam 带有一个跨平台 IDE（基于 Qt Creator），专门用于支持可编程相机。IDE 允许查看相机的帧缓冲区、访问传感器控件、通过 USB 串行（或 WiFi/BLE，如果可用）将脚本上传到相机，并包含一组图像处理工具来生成标签、阈值、关键点等...</font></font></p>
<p align="center" dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://raw.githubusercontent.com/openmv/openmv-media/master/boards/openmv-cam/v3/web-new-cam-v3-angle.jpg"><img width="320" src="https://raw.githubusercontent.com/openmv/openmv-media/master/boards/openmv-cam/v3/web-new-cam-v3-angle.jpg" style="max-width: 100%;"></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第一代 OpenMV 摄像头基于 STM32 ARM Cortex-M 数字信号处理器 (DSP) 和 OmniVision 传感器。这些电路板内置 RGB 和 IR LED、用于编程和视频流的 USB FS 支持、uSD 插槽以及 I/O 接头，可分出 PWM、UART、SPI、I2C、CAN 等。此外，OpenMV Cam 还支持使用 I/O 接头的扩展模块（屏蔽），以添加 WiFi 适配器、LCD 显示器、热视觉传感器、电机驱动器等。OpenMV 项目于 2015 年通过 Kickstarter 成功获得资助，自那时以来取得了长足进步。有关更多信息，请访问</font></font><a href="https://openmv.io" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://openmv.io</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TensorFlow 支持</font></font></h2><a id="user-content-tensorflow-support" class="anchor" aria-label="永久链接：TensorFlow 支持" href="#tensorflow-support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenMV 固件支持加载量化的 TensorFlow Lite 模型。固件支持将驻留在文件系统上的外部模型加载到内存（在带有 SDRAM 的主板上），并将内部模型（嵌入到固件中）加载到位。要从 Python 的文件系统加载外部 TensoFlow 模型，请使用</font></font><a href="https://docs.openmv.io/library/omv.tf.html" rel="nofollow"><code>tf</code></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python 模块。有关将 TensorFlow 模型嵌入固件并加载它们的信息，请参阅</font></font><a href="https://github.com/openmv/openmv/blob/master/src/lib/libtf/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TensorFlow 支持</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">接口库</font></font></h2><a id="user-content-interface-library" class="anchor" aria-label="永久链接：界面库" href="#interface-library"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenMV Cam 内置 RPC（远程 Python/过程调用）库，可轻松将 OpenMV Cam 连接到您的计算机、RaspberryPi 或 Beaglebone 等 SBC（单板计算机）或 Arduino 或 ESP8266/32 等微控制器。RPC 接口库的工作方式如下：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">异步串行（UART）-在 OpenMV Cam H7 上</font><font style="vertical-align: inherit;">速度高达</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7.5 Mb/s 。</font></font></strong><font style="vertical-align: inherit;"></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">I2C 总线 -在 OpenMV Cam H7 上
</font><font style="vertical-align: inherit;">最高可达</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1 Mb/s 。</font></font></strong><font style="vertical-align: inherit;"></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用1K上拉电阻。</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SPI 总线 -在 OpenMV Cam H7 上
</font><font style="vertical-align: inherit;">最高可达</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">20 Mb/s 。</font></font></strong><font style="vertical-align: inherit;"></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用足够短的电线，可以实现</font><font style="vertical-align: inherit;">高达</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">80 Mb/s</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">40 Mb/s 。</font></font></strong><font style="vertical-align: inherit;"></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CAN 总线 -在 OpenMV Cam H7 上</font><font style="vertical-align: inherit;">最高可达</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1 Mb/s 。</font></font></strong><font style="vertical-align: inherit;"></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">USB 虚拟 COM 端口 (VCP) -在 OpenMV Cam M4/M7/H7 上</font><font style="vertical-align: inherit;">最高可达</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12 Mb/s 。</font></font></strong><font style="vertical-align: inherit;"></font></li>
<li><font style="vertical-align: inherit;"></font><a href="https://openmv.io/collections/shields/products/wifi-shield-1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用WiFi Shield 的</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WiFi -</font><font style="vertical-align: inherit;">在 OpenMV Cam M4/M7/H7 上</font><font style="vertical-align: inherit;">最高可达</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12 Mb/s 。</font></font></strong><font style="vertical-align: inherit;"></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 RPC 库，您可以轻松获得图像处理结果、传输 RAW 或 JPG 图像数据，或者让 OpenMV Cam 控制另一个微控制器进行驱动电机等低级硬件控制。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><code>File-&gt;Examples-&gt;Remote Control</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以在 OpenMV IDE 和在线</font></font><a href="/openmv/openmv/blob/master/scripts/examples/34-Remote-Control"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此处</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">找到在 OpenMV Cam 上运行的示例</font><font style="vertical-align: inherit;">。最后，OpenMV 提供了以下库，用于将您的 OpenMV Cam 连接到以下其他系统：</font></font></p>
<ul dir="auto">
<li><a href="/openmv/openmv/blob/master/tools/rpc/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">适用于 USB VCP、以太网/WiFi、UART、Kvarser CAN 和 I2C/SPI 通信的通用 Python 接口库</font></font></a>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供用于将您的 OpenMV Cam 连接到 Windows、Mac 或 Linux 计算机（或 RaspberryPi/Beaglebone 等）的 Python 代码。
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">所有系统均支持 USB VCP。例如，直接通过 USB 连接到 OpenMV Cam。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">所有系统都支持以太网/WiFi。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持所有系统上的 RS232/RS422/RS485/TTL UART。例如，PC 背面的老式 DB9 端口、USB 转串行 RS232/RS422/RS485/TTL 适配器，以及 RaspberryPi/Beaglebone 等 SBC 上的 TTL 串行 I/O 引脚。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持 Windows 和 Linux 上的 Kvarser CAN（Kvarser 不支持 Mac）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持 RaspberryPi/Beaglebone 等 Linux SBC 上的 I2C/SPI（即将推出）</font></font></li>
</ul>
</li>
</ul>
</li>
<li><a href="https://github.com/openmv/openmv-arduino-rpc"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于 CAN、I2C、SPI、UART 通信的 Arduino 接口库</font></font></a>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">适用于所有 Arduino 变体。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过 SPI 上的 MCP2515 或通过 ESP32 上的 CAN 外设支持 CAN。</font></font></li>
</ul>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">串行端口注意事项</font></font></h4><a id="user-content-note-on-serial-port" class="anchor" aria-label="永久链接：串行端口注意事项" href="#note-on-serial-port"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您只需要</font></font><code>print()</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过 USB 读取在 OpenMV 相机上运行的脚本的输出，那么您只需要打开 OpenMV 相机虚拟 COM 端口并从串行端口读取文本行。例如（使用</font></font><a href="https://pythonhosted.org/pyserial/index.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">pyserial</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">）：</font></font></p>
<div class="highlight highlight-source-python notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">import</span> <span class="pl-s1">serial</span>
<span class="pl-s1">ser</span> <span class="pl-c1">=</span> <span class="pl-s1">serial</span>.<span class="pl-v">Serial</span>(<span class="pl-s">"COM3"</span>, <span class="pl-s1">timeout</span><span class="pl-c1">=</span><span class="pl-c1">1</span>, <span class="pl-s1">dsrdtr</span><span class="pl-c1">=</span><span class="pl-c1">False</span>)
<span class="pl-k">while</span> <span class="pl-c1">True</span>:
    <span class="pl-s1">line</span> <span class="pl-c1">=</span> <span class="pl-s1">ser</span>.<span class="pl-en">readline</span>().<span class="pl-en">strip</span>()
    <span class="pl-k">if</span> <span class="pl-s1">line</span>: <span class="pl-en">print</span>(<span class="pl-s1">line</span>)</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="import serial
ser = serial.Serial(&quot;COM3&quot;, timeout=1, dsrdtr=False)
while True:
    line = ser.readline().strip()
    if line: print(line)" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上述代码适用于 Windows、Mac 或 Linux。您只需将上述端口名称更改为 OpenMV Cam 显示的 USB VCP 端口的相同名称（它将在</font></font><code>/dev/</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mac 或 Linux 下）。请注意，如果您使用其他串行库和/或语言打开 USB VCP 端口，请确保将 DTR 行设置为 false - 否则 OpenMV Cam 将抑制打印输出。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从源代码构建固件</font></font></h2><a id="user-content-building-the-firmware-from-source" class="anchor" aria-label="永久链接：从源代码构建固件" href="#building-the-firmware-from-source"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">修补固件并重建固件的最简单方法是分叉此存储库，在分叉存储库中启用操作（从操作选项卡），然后推送更改。我们的 GitHub 工作流程在推送到主分支和/或合并拉取请求时重建固件，并生成一个开发版本，每个支持的主板都附带单独的固件包。有关更复杂的更改以及从源代码本地构建 OpenMV 固件，请参阅</font></font><a href="https://github.com/openmv/openmv/blob/master/src/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从源代码构建固件</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为项目做贡献</font></font></h2><a id="user-content-contributing-to-the-project" class="anchor" aria-label="永久链接：为项目做贡献" href="#contributing-to-the-project"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">欢迎大家积极贡献。如果您有兴趣为该项目做出贡献，请先创建以下每个存储库的分支：</font></font></p>
<ul dir="auto">
<li><a href="https://github.com/openmv/openmv.git"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/openmv/openmv.git</font></font></a></li>
<li><a href="https://github.com/openmv/micropython.git"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://github.com/openmv/micropython.git</font></font></a></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克隆分叉的 openmv 存储库，并将远程存储库添加到主 openmv 存储库：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>git clone --recursive https://github.com/<span class="pl-k">&lt;</span>username<span class="pl-k">&gt;</span>/openmv.git
git -C openmv remote add upstream https://github.com/openmv/openmv.git</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="git clone --recursive https://github.com/<username>/openmv.git
git -C openmv remote add upstream https://github.com/openmv/openmv.git" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"></font><code>origin</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将micropython 子模块的远程</font><font style="vertical-align: inherit;">设置为分叉的 micropython repo：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>git -C openmv/src/micropython remote set-url origin https://github.com/<span class="pl-k">&lt;</span>username<span class="pl-k">&gt;</span>/micropython.git</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="git -C openmv/src/micropython remote set-url origin https://github.com/<username>/micropython.git" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最后向 openmv 的 micropython fork 添加一个远程服务器：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>git -C openmv/src/micropython remote add upstream https://github.com/openmv/micropython.git</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="git -C openmv/src/micropython remote add upstream https://github.com/openmv/micropython.git" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现在，存储库已准备好接受拉取请求。要发送拉取请求，请创建一个新的功能分支并将其推送到原点，然后使用 Github 从分叉存储库创建拉取请求到上游 openmv/micropython 存储库。例如：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>git checkout -b <span class="pl-k">&lt;</span>some_branch_name<span class="pl-k">&gt;</span>
<span class="pl-k">&lt;</span>commit changes<span class="pl-k">&gt;</span>
git push origin -u <span class="pl-k">&lt;</span>some_branch_name<span class="pl-k">&gt;</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="git checkout -b <some_branch_name>
<commit changes>
git push origin -u <some_branch_name>" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">贡献指南</font></font></h3><a id="user-content-contribution-guidelines" class="anchor" aria-label="永久链接：贡献指南" href="#contribution-guidelines"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向上游发送拉取请求时，请遵循</font></font><a href="https://developers.google.com/blockly/guides/modify/contribute/write_a_good_pr" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最佳做法</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。一般来说，拉取请求应该：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解决一个问题。不要试图一次解决多个问题。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 git commit 将更改分成逻辑组。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">拉取请求标题应少于 78 个字符，并符合以下模式：
</font></font><ul dir="auto">
<li><code>&lt;scope&gt;:&lt;1 space&gt;&lt;description&gt;&lt;.&gt;</code></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提交主题行应少于 78 个字符，并符合以下模式：
</font></font><ul dir="auto">
<li><code>&lt;scope&gt;:&lt;1 space&gt;&lt;description&gt;&lt;.&gt;</code></li>
</ul>
</li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PR 标题或提交主题行示例：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>github: Update workflows.
Libtf: Add support for built-in models.
RPC library: Remove CAN bit timing function.
OPENMV4: Add readme template file.
ports/stm32/main.c: Fix storage label.
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="github: Update workflows.
Libtf: Add support for built-in models.
RPC library: Remove CAN bit timing function.
OPENMV4: Add readme template file.
ports/stm32/main.c: Fix storage label." tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</article></div>
